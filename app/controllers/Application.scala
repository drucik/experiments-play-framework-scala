package controllers

import models.Task
import play.api.data.Forms._
import play.api.data._
import play.api.libs.json._
import play.api.mvc._

object Application extends Controller {

  val taskForm = Form(
    "label" -> nonEmptyText
  )

  implicit val rds = (
    (__ \ 'label).read[String]
  )

  implicit val taskWrites = new Writes[Task] {
    def writes(task: Task): JsValue = {
      Json.obj(
        "label" -> task.label
      )
    }
  }

  def index = Action {
    Redirect(routes.Application.tasks)
  }

  def tasks = Action {
    Ok(views.html.index(Task.all(), taskForm))
  }

  def newTask = Action(parse.json) { request =>
    request.body.validate[(String)].map {
      case (label) => {
        Task.create(label)
        Ok(Json.toJson(Task.all()))
      }
    }.recoverTotal {
      e => BadRequest(JsError.toFlatJson(e))
    }
  }


  def newTask_old = Action { implicit request =>
    taskForm.bindFromRequest().fold(
      errors => BadRequest(views.html.index(Task.all(), errors)),
      label => {
        Task.create(label)
        Redirect(routes.Application.tasks())
      }
    )
  }

  def deleteTask(id: Long) = Action {
    Task.delete(id)
    Redirect(routes.Application.tasks())
  }

}