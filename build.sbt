import play.Project._

name := "my-first-scala-play"

version := "1.0"

playScalaSettings

libraryDependencies ++= Seq(
  jdbc,
  anorm
)
